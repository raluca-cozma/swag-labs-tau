# Proiect final pentru testarea aplicației Swag Labs

## Scurtă descriere

### Proiectul testează următoarele functionalități ale aplicației:

- Autentificare
- Deconectare
- Pagini
- Produse
- Imagini produse

### Tehnologii utilizate

- Java 17
- Maven
- Selenide Framework
- Allure report
- TestNg

### Utilizatori

- standard_user
- problem_user
- performance_glitch_user

**Parola comună:** secret_sauce

### Data providers

- User Data Provider
- Product Data Provider
