package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginBox {
    SelenideElement box = $(".login-box");
    SelenideElement userField = $("#user-name");
    SelenideElement passwordField = $("#password");
    SelenideElement loginButton = $("#login-button");
    SelenideElement errorMsg = $(".error h3");
    String lockedOutUserMsg = "Epic sadface: Sorry, this user has been locked out.";

    public SelenideElement getBox() {
        return box;
    }

    public void fillInUsername(String user) {
        userField.sendKeys(user);
    }

    public void fillInPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void clickOnLoginButton() {
        loginButton.click();
    }

    public String getErrorMsg() {
        return errorMsg.text();
    }

    public String getLockedOutUserMsg() {
        return lockedOutUserMsg;
    }

}
