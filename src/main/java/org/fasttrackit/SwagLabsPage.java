package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class SwagLabsPage extends Page {
    SelenideElement menuButton = $("#react-burger-menu-btn");
    SelenideElement logoutButton = $("#logout_sidebar_link");
    public String swabLabInventory = "https://www.saucedemo.com/inventory.html";

    public void clickOnMenuButton() {
        menuButton.click();
    }

    public void clickOnLogoutButton() {
        logoutButton.click();
    }

    public String getSwabLabInventory() {
        return swabLabInventory;
    }
}

