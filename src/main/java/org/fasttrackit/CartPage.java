package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class CartPage extends Page {
    String cartPageUrl = "https://www.saucedemo.com/cart.html";

    private SelenideElement cartBadge = $(By.className("shopping_cart_badge"));
    private SelenideElement cartItem = $(By.className("cart_item"));
    private SelenideElement removeButton = cartItem.$("[id^='remove']");
    private SelenideElement continueShopping = $(By.id("continue-shopping"));
    private SelenideElement checkOutButton = $("#checkout");

    public SelenideElement getCartBadge() {
        return cartBadge;
    }

    public void openCartPage() {
        Selenide.open(cartPageUrl);
    }

    public void removeFromCart() {
        removeButton.click();
    }

    public void continueShopping() {
        continueShopping.click();
    }

    public void clickOnCheckOutButton() {
        checkOutButton.click();
    }
}
