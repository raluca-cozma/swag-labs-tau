package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductSauceLabBackpack {
    private SelenideElement sauceLabsBackpack = $("#item_4_title_link");
    private SelenideElement addToCartButton = $("#add-to-cart-sauce-labs-backpack");
    ;

    public SelenideElement getSauceLabsBackpack() {
        return sauceLabsBackpack;
    }

    public SelenideElement getAddToCartButton() {
        return addToCartButton;
    }

    public void clickOnAddToCartButton() {
        addToCartButton.click();
    }
}
