package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class Product {
    String name;
    String price;
    private final String defaultImageSrc = "https://www.saucedemo.com/static/media/sl-404.168b1cce.jpg";
    private SelenideElement productLink;

    private SelenideElement addToCart;

    private SelenideElement removeFromCart;
    private SelenideElement image = $("img.inventory_item_img");
    private SelenideElement addToCartbag = $("#add-to-cart-sauce-labs-backpack");

    public Product(String itemId, String name, String price) {
        String productSelector = String.valueOf($(By.id("#item_%s_title_link")));
        this.name = name;
        this.price = price;
        this.productLink = $(By.id(productSelector));

        addToCart = $("#add-to-cart-" + this.name.toLowerCase().replace(" ", "-"));
        removeFromCart = $("#remove-" + this.name.toLowerCase().replace(" ", "-"));
    }

    public void clickOnAddToCart() {
        addToCart.click();
    }

    public void clickOnRemoveFromCart() {
        removeFromCart.click();
    }

    public void clickOnAddToBag() {
        addToCartbag.click();
    }

    public SelenideElement getImage() {
        return image;
    }

    public String getDefaultImageSrc() {
        return defaultImageSrc;
    }
}