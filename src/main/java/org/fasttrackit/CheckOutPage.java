package org.fasttrackit;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckOutPage extends Page {
    String checkOutPageStepOne = "https://www.saucedemo.com/checkout-step-one.html";
    String checkOutPageStepTwo = "https://www.saucedemo.com/checkout-step-two.html";
    String getCheckOutPageComplete = "https://www.saucedemo.com/checkout-complete.html";
    private SelenideElement cancelButton = $("#cancel");

    public void open() {
        Selenide.open(checkOutPageStepOne);
    }

    public void openStepTwo() {
        Selenide.open(checkOutPageStepTwo);
    }

    public void openPageComplete() {
        Selenide.open(getCheckOutPageComplete);
    }

    public void clickOnCancelButton() {
        cancelButton.click();
    }
}


