package org.fasttrackit;

import com.codeborne.selenide.Selenide;

public class Page {
    private String swagLabsUrl = "https://www.saucedemo.com/";

    public Page() {
        System.out.println("Opened a new page.");
    }

    public void openSwagLabsUrl() {
        System.out.println("Opening: " + swagLabsUrl);
        Selenide.open(swagLabsUrl);
    }
}