package orgfasttrackit.data.dataprovider;

import org.testng.annotations.DataProvider;

public class UserDataProvider {
    @DataProvider(name = "validUserDataProvider")
    public static Object[][] fillInUserDataProvider() {
        User standardUser = new User("standard_user", "secret_sauce");
        User problemUser = new User("problem_user", "secret_sauce");
        User performanceUser = new User("performance_glitch_user", "secret_sauce");

        return new Object[][]{
                {standardUser},
                {problemUser},
                {performanceUser}
        };
    }

    @DataProvider(name = "inValidUserDataProvider")
    public static Object[][] fillInUserDataProviderInvalid() {
        User lockedOutUser = new User("locked_out_user", "secret_sauce");

        return new Object[][]{
                {lockedOutUser},
        };
    }
}