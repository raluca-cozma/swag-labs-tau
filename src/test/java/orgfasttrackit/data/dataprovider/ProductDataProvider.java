package orgfasttrackit.data.dataprovider;

import org.fasttrackit.Product;
import org.testng.annotations.DataProvider;

public class ProductDataProvider {
    @DataProvider(name = "allProductsDataProvider")
    public static Object[][] fillInProducts() {
        Product backPack = new Product("4", "Sauce Labs Backpack", "29.99");
        Product bikeLight = new Product("0", "Sauce Labs Bike light", "9.99");
        Product boltTShirt = new Product("1", "Sauce Labs Bolt T-Shirt", "15.99");
        Product fleeceJacket = new Product("5", "Sauce Labs Fleece Jacket", "49.99");
        Product onesie = new Product("2", "Sauce Labs Onesie", "7.99");
//        Product redTShirt = new Product("3","Test.allTheThings() T-Shirt (Red)" ,"15.99");

        return new Object[][]{
                {backPack},
                {bikeLight},
                {boltTShirt},
                {fleeceJacket},
                {onesie},
//                {redTShirt}
        };
    }

    @DataProvider(name = "firstFourProductsDataProvider")
    public static Object[][] firstFourProducts() {
        Product backPack = new Product("4", "Sauce Labs Backpack", "29.99");
        Product bikeLight = new Product("0", "Sauce Labs Bike light", "9.99");
        Product boltTShirt = new Product("1", "Sauce Labs Bolt T-Shirt", "15.99");
        Product fleeceJacket = new Product("5", "Sauce Labs Fleece Jacket", "49.99");

        return new Object[][]{
                {backPack},
                {bikeLight},
                {boltTShirt},
                {fleeceJacket},
        };
    }
}
