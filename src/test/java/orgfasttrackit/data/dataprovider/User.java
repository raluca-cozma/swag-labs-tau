package orgfasttrackit.data.dataprovider;

public class User {
    private final String username;
    private final String password;
    private String errorMessage = "Epic sadface: Username is required";
    private String lockedErrorMsg = "Epic sadface: Sorry, this user has been locked out.";
    private String wrongUser = "Epic sadface: Username and password do not match any user in this service";


    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
