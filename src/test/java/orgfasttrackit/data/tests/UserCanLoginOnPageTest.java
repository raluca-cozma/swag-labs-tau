package orgfasttrackit.data.tests;

import io.qameta.allure.Feature;
import org.fasttrackit.LoginBox;
import org.fasttrackit.SwagLabsPage;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import orgfasttrackit.configuration.BaseTestConfig;
import orgfasttrackit.data.dataprovider.User;
import orgfasttrackit.data.dataprovider.UserDataProvider;

import static org.testng.Assert.assertEquals;

@Feature("User Login")
public class UserCanLoginOnPageTest extends BaseTestConfig {
    SwagLabsPage page;

    @BeforeMethod
    public void openPage() {
        page = new SwagLabsPage();
        page.openSwagLabsUrl();
    }

    @AfterMethod
    public void logout() {
        page.clickOnMenuButton();
        page.clickOnLogoutButton();
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    public void userCanLoginOnSwagLabsPage(User user) {
        LoginBox loginBox = new LoginBox();
        loginBox.fillInUsername(user.getUsername());
        loginBox.fillInPassword(user.getPassword());
        loginBox.clickOnLoginButton();
        assertEquals(page.swabLabInventory, page.getSwabLabInventory(), "Swab Labs Inventory page is opened");
    }
}
