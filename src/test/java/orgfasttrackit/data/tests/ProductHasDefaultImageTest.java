package orgfasttrackit.data.tests;

import org.fasttrackit.LoginBox;
import org.fasttrackit.Product;
import org.fasttrackit.SwagLabsPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import orgfasttrackit.configuration.BaseTestConfig;
import orgfasttrackit.data.dataprovider.ProductDataProvider;

public class ProductHasDefaultImageTest extends BaseTestConfig {
    SwagLabsPage page;

    @BeforeClass
    public void openPage() {
        page = new SwagLabsPage();
        page.openSwagLabsUrl();

        LoginBox loginBox = new LoginBox();
        loginBox.fillInUsername("problem_user");
        loginBox.fillInPassword("secret_sauce");
        loginBox.clickOnLoginButton();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "allProductsDataProvider")
    public void productHasDefaultImage(Product product) {
        Assert.assertEquals(product.getImage().getAttribute("src"), product.getDefaultImageSrc(), "Product has default image");

    }
}
