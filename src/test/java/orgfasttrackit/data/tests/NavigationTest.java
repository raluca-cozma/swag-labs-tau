package orgfasttrackit.data.tests;

import io.qameta.allure.Feature;
import org.fasttrackit.CartPage;
import org.fasttrackit.CheckOutPage;
import org.fasttrackit.LoginBox;
import org.fasttrackit.SwagLabsPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import orgfasttrackit.configuration.BaseTestConfig;

import static com.codeborne.selenide.WebDriverRunner.url;
import static org.testng.Assert.assertEquals;

@Feature("user can navigate through different pages")
public class NavigationTest extends BaseTestConfig {
    SwagLabsPage page = new SwagLabsPage();
    CartPage cartPage = new CartPage();
    CheckOutPage checkOutPage = new CheckOutPage();

    @BeforeClass
    public void openPage() {
        page.openSwagLabsUrl();

        LoginBox loginBox = new LoginBox();
        loginBox.fillInUsername("standard_user");
        loginBox.fillInPassword("secret_sauce");
        loginBox.clickOnLoginButton();
    }

    @Test
    public void userIsOnInventoryPageAfterLogin() {
        String expectedUrl = "https://www.saucedemo.com/inventory.html";
        String actualUrl = url();
        assertEquals(actualUrl, expectedUrl);
    }

    @Test
    public void userOpensCartPage() {
        cartPage.openCartPage();

        String expectedUrl = "https://www.saucedemo.com/cart.html";
        String actualUrl = url();
        assertEquals(actualUrl, expectedUrl);
    }

    @Test
    public void userOpensCheckoutPageStepOne() {
        checkOutPage.open();

        String expectedUrl = "https://www.saucedemo.com/checkout-step-one.html";
        String actualUrl = url();
        assertEquals(actualUrl, expectedUrl);
    }

    @Test
    public void userOpensCheckoutPageStepTwo() {
        checkOutPage.openStepTwo();

        String expectedUrl = "https://www.saucedemo.com/checkout-step-two.html";
        String actualUrl = url();
        assertEquals(actualUrl, expectedUrl);
    }

    @Test
    public void userOpensCheckoutPageStepComplete() {
        checkOutPage.openPageComplete();

        String expectedUrl = "https://www.saucedemo.com/checkout-complete.html";
        String actualUrl = url();
        assertEquals(actualUrl, expectedUrl);
    }
}
