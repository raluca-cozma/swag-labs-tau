package orgfasttrackit.data.tests;

import io.qameta.allure.Feature;
import org.fasttrackit.CartPage;
import org.fasttrackit.LoginBox;
import org.fasttrackit.Product;
import org.fasttrackit.SwagLabsPage;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import orgfasttrackit.data.dataprovider.ProductDataProvider;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;

@Feature("user can add all products to cart")
public class LoggedUserCanAddAllProductsToCartTest {
    SwagLabsPage page;

    Integer cartItemCount = 0;

    CartPage cartPage = new CartPage();

    @BeforeClass
    public void openPage() {
        page = new SwagLabsPage();
        page.openSwagLabsUrl();

        LoginBox loginBox = new LoginBox();
        loginBox.fillInUsername("standard_user");
        loginBox.fillInPassword("secret_sauce");
        loginBox.clickOnLoginButton();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "allProductsDataProvider")
    public void userCanAddAllProductsToCart(Product product) {
        product.clickOnAddToCart();
        cartItemCount++;

        assertEquals(
                cartPage.getCartBadge().text(),
                cartItemCount.toString(),
                "Cart badge shows " + cartItemCount + " product in cart"
        );
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "allProductsDataProvider")
    public void userCanRemoveAllProductsFromCart(Product product) {
        product.clickOnRemoveFromCart();
        cartItemCount--;

        if (cartItemCount > 0) {
            assertEquals(
                    cartPage.getCartBadge().text(),
                    cartItemCount.toString(),
                    "Cart badge shows " + cartItemCount + " product in cart"
            );
        } else {
            assertFalse(
                    cartPage.getCartBadge().isDisplayed(),
                    "Cart badge is not shown"
            );
        }
    }
}
