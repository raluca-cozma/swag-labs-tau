package orgfasttrackit.data.tests;

import io.qameta.allure.Feature;
import org.fasttrackit.*;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import orgfasttrackit.configuration.BaseTestConfig;

import static com.codeborne.selenide.WebDriverRunner.url;
import static org.testng.Assert.assertEquals;

@Feature("Logged user can click on checkout from cart page")
public class LoggerUserCanCheckOutFromCartPageTest extends BaseTestConfig {
    SwagLabsPage page;
    CartPage cartPage = new CartPage();
    CheckOutPage checkOutPage = new CheckOutPage();

    @BeforeClass
    public void openPage() {
        page = new SwagLabsPage();
        page.openSwagLabsUrl();

        LoginBox loginBox = new LoginBox();
        loginBox.fillInUsername("standard_user");
        loginBox.fillInPassword("secret_sauce");
        loginBox.clickOnLoginButton();
    }

    @Test()
    public void userCanCheckoutFromCartPage() {
        Product sauceLabBackpack = new Product("4", "Sauce Labs Backpack", "29.99");
        sauceLabBackpack.clickOnAddToBag();
        cartPage.openCartPage();
        cartPage.clickOnCheckOutButton();

        String expectedUrl = "https://www.saucedemo.com/checkout-step-one.html";
        String actualUrl = url();
        assertEquals(actualUrl, expectedUrl);
    }
}
