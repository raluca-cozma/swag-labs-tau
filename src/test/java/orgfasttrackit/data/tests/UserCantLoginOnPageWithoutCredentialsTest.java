package orgfasttrackit.data.tests;

import org.fasttrackit.LoginBox;
import org.fasttrackit.SwagLabsPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import orgfasttrackit.configuration.BaseTestConfig;
import orgfasttrackit.data.dataprovider.User;
import orgfasttrackit.data.dataprovider.UserDataProvider;

import static org.testng.Assert.assertEquals;


public class UserCantLoginOnPageWithoutCredentialsTest extends BaseTestConfig {
    SwagLabsPage page;

    @BeforeMethod
    public void setup() {
        page = new SwagLabsPage();
        page.openSwagLabsUrl();
    }

    @Test
    public void userCannotLoginWithoutEnteringUsernameAndPassword() {
        LoginBox loginBox = new LoginBox();
        loginBox.clickOnLoginButton();
        assertEquals(loginBox.getBox().text(), loginBox.getErrorMsg(), "Error message is: Epic sadface: Username is required");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "inValidUserDataProvider")
    public void lockedOutUserCannotLoginOnSwabsLabsPAge(User user) {
        LoginBox loginBox = new LoginBox();
        loginBox.fillInUsername(user.getUsername());
        loginBox.fillInPassword(user.getPassword());
        loginBox.clickOnLoginButton();
        assertEquals(loginBox.getBox().text(), loginBox.getLockedOutUserMsg(), "Epic sadface: Sorry, this user has been locked out.");
    }
}
