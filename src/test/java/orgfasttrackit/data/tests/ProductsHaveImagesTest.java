package orgfasttrackit.data.tests;

import io.qameta.allure.Feature;
import org.fasttrackit.LoginBox;
import org.fasttrackit.Product;
import org.fasttrackit.SwagLabsPage;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import orgfasttrackit.configuration.BaseTestConfig;
import orgfasttrackit.data.dataprovider.ProductDataProvider;

import static org.testng.Assert.assertTrue;

@Feature("All products have images")
public class ProductsHaveImagesTest extends BaseTestConfig {
    SwagLabsPage page;

    @BeforeClass
    public void openPage() {
        page = new SwagLabsPage();
        page.openSwagLabsUrl();

        LoginBox loginBox = new LoginBox();
        loginBox.fillInUsername("standard_user");
        loginBox.fillInPassword("secret_sauce");
        loginBox.clickOnLoginButton();
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "allProductsDataProvider")
    public void productHasTheCorrectImage(Product product) {
        assertTrue(
                product.getImage().isDisplayed(),
                "Product image appears"
        );
    }

    @Test(dataProviderClass = ProductDataProvider.class, dataProvider = "allProductsDataProvider")
    public void productHasCustomImage(Product product) {
        Assert.assertNotEquals(product.getImage().getAttribute("src"), product.getDefaultImageSrc(), "Product has custom image");
    }
}