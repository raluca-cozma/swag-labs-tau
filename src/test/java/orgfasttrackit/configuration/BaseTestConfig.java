package orgfasttrackit.configuration;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.testng.annotations.AfterTest;

public class BaseTestConfig {
    public BaseTestConfig() {
        Configuration.browser = "Chrome";
        Configuration.headless = false;
    }

    @AfterTest
    public void closeBrowser() {
        Selenide.closeWindow();
    }
}
